# Teaching Machine Learning at ICML2020

## Abstract

Machine Learning based approaches have become ubiquitous in many areas of society, industry and academia. Understanding what Machine Learning is, providing and reproducing what it infers, has become an essential prerequisite for adoption. In this line of thought, course materials, introductory media and lecture series of a broad variety, depth, quality and public availability have come to existence. To this date and to the best our knowledge, there is no structured approach to collect and discuss best practices in teaching Machine Learning. This workshop strives to change this. With our workshop, we want to start an academic discussion on what works and what doesn't in order to help improve existing material and to make conceiving new material more effective.
