## Tentative Timetable

| time     | Title                                          | Speaker             |
| --:      | :--                                            | :--                 |
| 9.00 am  | Welcome                                        | Organizers          |
| 9.30 am  | Didactics of Data                              | Unconfirmed Speaker |
| 10.00 am | Experiences with MOOCs                         | Unconfirmed Speaker |
| 10.30 am | Coffee break                                   |                     |
| 11.00 am | Experiences in Lectures                        | Unconfirmed Speaker |
| 11.30 am | Experiences in Bootcamps                       | Unconfirmed Speaker |
| 12.00 am | Lunch                                          |                     |
| 1.00 pm  | Preface (Parallel) Teaching Example Session    | Organizers          |
| 1.30 pm  | How to give feedback                           | Organizers          |
| 2.15 pm  | Teaching Example Presentations                 | All                 |
| 2.45 pm  | Coffee break                                   |                     |
| 3.15 pm  | Teaching Example Presentations                 | All                 |
| 5.00 pm  | Coffee break                                   |                     |
| 5.30 pm  | Summary Teaching Example Presentations         | All                 |
| 6.00 pm  | Farewell and Next Steps                        | Organizers          |
| 6.30 pm  | End                                            |                     |
