## Topics Covered

The main goal of this workshop is to motivate and nourish best practices at any stage of the teaching process. For this, we would like to cover a structured approach to teaching motivated by [the carpentries][cdh] or a variation thereof. As we believe that core concepts contained in this are vital for any teaching practitioners, we will discuss these during the workshop. Aspects of these include: course context, pre- and post-workshop surveys, learner profiles, learning goals and objectives, giving teaching feedback. With this, we hope to equip attendees with a structured approach to teaching.

The central activity of the workshop will be a (potentially parallel) **presentation of teaching examples** from the field. These **teaching examples will be collected from submissions to the call-for-papers**. We hope to attract 2-3 page mini articles that present a teaching activity related to machine learning. This could be a discussion of

* a demo of how to teach back propagation
* a discussion of an instructive data set for teaching deep learning
* an interactive web application to play with parameters of a classifier
* a teaching metaphor to illustrate time series prediction

to name a few. Depending on the room and number of submissions, we will divide the presentations based on the field they focus on: vision applications, test and language applications, general concepts etc. Each of these working groups is asked to collect general patters on what works and what doesn't. The workshop will only set a loose limit to the topics presented, i.e. we want to focus on teaching modern classifiers from classical and Deep Learning based methods.
