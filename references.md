## References

This is a placeholder in case we want to add a bibliography of some sort. For now, it summarizes markdown links which are not yet rendered to the pdf. See the raw content of [references.md](references.md) for details.

[cdh]: https://cdh.carpentries.org/
